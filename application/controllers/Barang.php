<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Barang extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library("form_validation");
			$this->load->model('barang_model');
		}

		public function index($data = array()){
			$res = $this->barang_model->get_item();
			$data = array(
				'items' => $res
			);

			$this->template_view->view('page/data_barang', $data, "Ubah Barang", 2);
		}

		public function change($id = NULL){
			if($id != NULL){
				if(count($_POST) > 0){
					$r = $this->barang_model->change_item($id);
					if($r){
						$this->template_view->text_view('Sukses mengubah barang', 'Sukses');
					}else{
						$this->template_view->text_view('Gagal mengubah barang', 'Gagal');
					}
				}else{
					$data = array('data' => $this->barang_model->get_item_by_id($id)[0], 'id' => $id);
					$this->template_view->view('page/ubah_barang', $data, 'Ubah Barang', 2);
				}
			}
		}

		public function delete($id = NULL){
			if($id != NULL){
				if($this->session->userdata('role') <= 2 && $this->session->userdata('role') != 0){
					$r = $this->barang_model->delete_item($id);
					if($r){
						$this->template_view->text_view('Sukses menghapus barang', 'Sukses');
					}else{
						$this->template_view->text_view('Gagal menghapus barang', 'Gagal');
					}
				}else{
					show_404();
				}
			}else{
				show_404();
			}
		}
	}
	
	/* End of file Tambah-barang.php */
	/* Location: ./application/controllers/Tambah-barang.php */