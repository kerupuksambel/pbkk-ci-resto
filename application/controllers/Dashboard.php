<?php  
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Dashboard extends CI_Controller {
	
		public function index(){
			$role = $this->session->userdata('role');
			$this->load->model('kas_model');

			$records = $this->kas_model->get_kas_list();
			$data = array(
				'name' => $this->session->userdata('user'),
				'records' => $records
			);

			// var_dump($data);
			if($role == 1){
				//CEO
				$this->template_view->view('dashboard/ceo', $data, 'Dashboard', 1);
			}else if($role == 2){
				//Admin
				$this->template_view->view('dashboard/admin', $data, 'Dashboard', 2);
			}else if($role == 3){
				//Kasir
				redirect(base_url('kasir'));
			}else{
				$this->template_view->view('page/error', NULL, 'Error!');
			}
		}
	
	}
	
	/* End of file Dashboard.php */
	/* Location: ./application/controllers/Dashboard.php */
?>