<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Kasir extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('meja_model');
		}

		public function index(){
			$tables = $this->meja_model->get_meja();
			$data = array(
				"tables" => $tables
			);

			$this->template_view->view('page/kasir', $data, 'Kasir', 3);
		}

		public function selesai($id = NULL){
			if($id != NULL){
				$nominal = $this->meja_model->tagihan_meja($id);
				$meja = $this->meja_model->nama_meja($id);
				if(isset($_POST['selesai'])){				
					//Transaksi selesai, masukkan input ke dalam kas
					//Redirect ke halaman utama pemesanan
					$this->load->model("kas_model");
					$r = $this->kas_model->insert_trx($nominal);
					if($r){
						$this->template_view->text_view("Sukses menyelesaikan transaksi.", "Sukses");
					}else{
						$this->template_view->text_view("Gagal menyelesaikan transaksi.", "Gagal");
					}
					$this->meja_model->empty_meja($id);
				}else{
					$nominal = $this->meja_model->tagihan_meja($id);
					$meja = $this->meja_model->nama_meja($id);
					$data = array(
						'nominal' => $nominal
					);

					$this->template_view->view("page/transaksi", $data, 'Selesaikan Transaksi');
				}

			}else{
				show_404();
			}
		}
	
	}
	
	/* End of file Kasir.php */
	/* Location: ./application/controllers/Kasir.php */