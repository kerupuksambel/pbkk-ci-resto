<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	require('./vendor/autoload.php');

	use PhpOffice\PhpSpreadsheet\Helper\Sample;
	use PhpOffice\PhpSpreadsheet\IOFactory;
	use PhpOffice\PhpSpreadsheet\Spreadsheet;

	class Laporan extends CI_Controller {
	
		public function __construct(){
			parent::__construct();
			$this->load->model('kas_model');
		}

		public function index(){
			$list = $this->kas_model->get_kas_list();
			$data = array(
				"list" => $list
			);
			$this->template_view->view('page/laporan', $data, 'Laporan', 2);
		}

		public function reset(){
			$role = $this->session->userdata('role');
			if($role <= 2 && $role != 0){
				$res = $this->kas_model->reset_kas();
				if($res){
					$this->template_view->text_view('Sukses mereset laporan', 'Sukses');
				}else{
					$this->template_view->text_view('Gagal mereset laporan', 'Gagal');
				}
			}else{
				show_404();
			}
		}

		public function printz(){
			$role = $this->session->userdata('role');
			if($role <= 2 && $role != 0){
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment; filename="laporan.csv"');
				$data = $this->kas_model->get_kas_list();
				$fp = fopen('php://output', 'wb');
				fputcsv($fp, array("Tanggal", "Pemasukan"));
				foreach ($data as $line) {
					$arr = array($line['tanggal'], $line['kas']);
				    fputcsv($fp, $arr);
				}
				fclose($fp);
			}else{
				show_404();
			}
		}

		public function print(){
			$data = $this->kas_model->get_kas_list();
			// Create new Spreadsheet object
			$spreadsheet = new Spreadsheet();

			// Add some data
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('A1', 'Tanggal')->setCellValue('B1', 'Pemasukan');

			// Miscellaneous glyphs, UTF-8
			$i=2; 
			foreach($data as $line) {
				$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $line['tanggal'])
				->setCellValue('B'.$i, $line['kas']);
				$i++;
			}

			// Rename worksheet
			$spreadsheet->getActiveSheet()->setTitle('Laporan');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$spreadsheet->setActiveSheetIndex(0);

			// Redirect output to a client’s web browser (Xlsx)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
			header('Cache-Control: max-age=0');

			$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
			$writer->save('php://output');
			exit;
		}
	}
	
	/* End of file Laporan.php */
	/* Location: ./application/controllers/Laporan.php */
?>