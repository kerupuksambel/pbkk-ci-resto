<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Login extends CI_Controller {
		public function __construct(){
			parent::__construct();
		}

		public function index($data = array()){
			if($this->session->userdata('verified') || $this->session->userdata('role') != NULL){
				redirect(base_url('dashboard'));
			}else{
				$this->template_view->public_view("login", $data, "Login");
			}
		}

		public function verify(){
			$this->load->model("login_model");
			$this->login_model->verify_login();

			$v = $this->session->verified;

			//	This process should be using redirection, as the member can only entered the member page once
			//right after they logged in

			if($v){
				$data = array(
					'nama_user' => $this->session->user
				);

				$this->index();
			}else{
				$data = array(
					'msg' => "Data Anda salah."
				);
				$this->index($data);
			}
		}
	}