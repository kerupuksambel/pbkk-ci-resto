<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Meja extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library("form_validation");
			$this->load->model('meja_model');
		}

		public function index($data = array()){
			$res = $this->meja_model->get_meja();
			$data = array(
				'users' => $res
			);

			$this->template_view->view('page/data_meja', $data, "Ubah Meja", 2);
		}

		public function change($id = NULL){
			if($id != NULL){
				if(count($_POST) > 0){
					$r = $this->meja_model->ubah_meja($id);
					if($r){
						$this->template_view->text_view('Sukses mengubah meja', 'Sukses');
					}else{
						$this->template_view->text_view('Gagal mengubah meja', 'Gagal');
					}
				}else{
					$data = array('nama' => $this->meja_model->nama_meja($id), 'id' => $id);
					$this->template_view->view('page/ubah_meja', $data, 'Ubah Meja', 2);
				}
			}else{
				show_404();
			}
		}

		public function delete($id = NULL){
			if($id != NULL){
				if($this->session->userdata('role') <= 2 && $this->session->userdata('role') != 0){
					$r = $this->meja_model->hapus_meja($id);
					if($r){
						$this->template_view->text_view('Sukses menghapus meja', 'Sukses');
					}else{
						$this->template_view->text_view('Gagal menghapus meja', 'Gagal');
					}
				}else{
					show_404();
				}
			}else{
				show_404();
			}
		}
	}
	
	/* End of file Tambah-barang.php */
	/* Location: ./application/controllers/Tambah-barang.php */