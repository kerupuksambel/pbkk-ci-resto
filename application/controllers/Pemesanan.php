<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Pemesanan extends CI_Controller {
	
		public function __construct(){
			parent::__construct();
		}

		public function index(){
			redirect(base_url("pemesanan/meja"));
		}

		public function meja(){
			$this->load->model("meja_model");

			$tables = $this->meja_model->get_meja_kosong();

			$data = array(
				'tables' => $tables, 
			);

			$this->template_view->public_view('page/meja', $data, 'Pesan Meja');
		}

		public function menu(){
			$this->load->library("form_validation");
			$this->load->model("barang_model");
			$this->load->model('meja_model');
			
			$list = $this->barang_model->get_item();
			$list_count = count($list);
			if(isset($_POST['meja'])){
				//Meja sudah diset
				if(count($_POST) > 1){
					//Request dari kembali dari struk
					$struk = $this->get_order_list();
					$printed = array();
					$total = 0;
					while(current($struk) != NULL){
						$substruk = current($struk);
						$id = $substruk['id'];
						$nama = $this->barang_model->get_item_by_id($id)[0]['nama_barang'];
						$harga = $this->barang_model->get_item_by_id($id)[0]['harga_barang'];
						$jumlah = $substruk['jumlah'];

						for($i = 0; $i < $list_count; $i++){
							if($list[$i]['id'] == $id){
								$list[$i]['jumlah'] = $jumlah;
							}
						}

						array_push($printed, array($nama, $jumlah, $harga, $jumlah * $harga));
						$total += $jumlah * $harga;
						next($struk);
					}
				}

				if($this->input->post('print') != NULL){
					$this->print($printed, $total);
					exit();
				}

				$nama_meja = $this->meja_model->nama_meja($_POST['meja']);
				// var_dump($_POST['meja']);
				$data = array(
					"items" => $list,
					"meja" => $_POST['meja'],
					"nama_meja" => $nama_meja
				);

				$this->template_view->public_view('page/pemesanan', $data, "Pesan Menu");

			}else{
				show_404();
			}
		}

		public function struk($method = NULL){
			$this->load->model("barang_model");
			$orders = $this->get_order_list();
			if($method == NULL){
				if(count($orders) > 0){
					$order_id = 0;
					$prints = array();
					foreach($orders as $order){
						$id = $order['id'];
						$item = $this->barang_model->get_item_by_id($id);
						if($item != NULL){
							$nama = $item[0]['nama_barang'];
							$harga = $item[0]['harga_barang'];
							$orders[$order_id]['nama_barang'] = $nama;
							$orders[$order_id]['harga_barang'] = $harga;
						}
						$order_id++;
					}

					$data = array(
						'orders' => $orders,
						'meja' => $_POST['meja']
					);

					$this->template_view->public_view("page/struk", $data, "Konfirmasi Pemesanan");
				}else{
					redirect(base_url("pemesanan/meja"));
				}
			}else{
				redirect(base_url("pemesanan/meja"));
			}
		}

		public function konfirmasi(){
			$this->load->model('meja_model');

			$meja = $this->input->post('meja');
			$total = $this->input->post('total');

			if(isset($meja)){
				$res = $this->meja_model->fill_meja($meja, $total);
				if($res){
					redirect('pemesanan');
				}else{
					show_404();
				}
			}else{
				show_404();
			}
		}

		private function get_order_list(){
			$inputs = $this->input->post();
			$orders = array();
			while(current($inputs) != NULL){
				$input = current($inputs);
				if($input > 0 && key($inputs) != "meja"){
					$order = array();
					
					$id = explode('_', key($inputs))[1];
					
					$order['id'] = $id;//nama dari id
					$order['jumlah'] = $input;

					array_push($orders, $order);
				}

				next($inputs);

			}
			
			return $orders;
		}
	
		public function print($list = NULL, $total = NULL){
			if($list != NULL){
				$role = $this->session->userdata('role');
				if($role <= 3){
					$this->load->library('pdf');
					$data = array("lists" => $list, 'total' => $total);

					//0 = Nama, 1 = Harga, 2 = Jumlah, 3 = Total, 4 = 

					$this->pdf->setPaper('A4', 'potrait');
					$this->pdf->filename = 'struk.pdf';
					$this->pdf->load_view('page/print', $data);
				}else{
					show_404();
				}
			}else{
				show_404();
			}
		}

	}
	
	/* End of file Pemesanan.php */
	/* Location: ./application/controllers/Pemesanan.php */