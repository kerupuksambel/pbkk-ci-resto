<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Tambah_barang extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library("form_validation");
		}

		public function index($data = array()){
			if(isset($_POST['nama']) && isset($_POST['harga'])){
				$this->load->model('barang_model');
				$stat = $this->barang_model->insert_item(); 

				if($stat['stat']){
					$data = array(
						"msg" => "Barang <b>".$stat['nama']."</b> berhasil dimasukkan."
					);
				}else{
					$data = array(
						"msg" => "Data gagal dimasukkan"
					);
				}
			}
			$this->template_view->view('page/tambah_barang', $data, 'Tambah Barang', 2);
		}

		public function meja(){
			$this->load->model('meja_model');
			if(count($_POST) > 0){
				$r = $this->meja_model->tambah_meja();
				if($r){
					$this->template_view->text_view('Sukses menambah meja', 'Sukses');
				}else{
					$this->template_view->text_view('Gagal menambah meja', 'Gagal');
				}
			}else{
				$this->template_view->view('page/tambah_meja', NULL, 'Tambah Meja', 2);
			}
		}
	}
	
	/* End of file Tambah-barang.php */
	/* Location: ./application/controllers/Tambah-barang.php */