<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Ubah_data extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('user_model');
		}

		public function index(){
			show_404();
		}

		public function admin($method = NULL, $id = NULL){
			if($method == "show"){
				$res = $this->user_model->get_user(2);
				$data = array(
					'users' => $res,
					'role' => 'admin'
				);

				$this->template_view->view('page/data_user', $data, "Data Admin", 1);
			}else if ($method == "change") {
				if(count($_POST) == 0 && $id != NULL){
					//Form Pengubahan
					$res = $this->user_model->get_specific_user($id);
					$data = array(
						'user' => $res,
						'role' => 'admin'
					);

					$this->template_view->view('page/ubah_user', $data, "Ubah Data Admin", 1);
				}else if($id != NULL){
					//Proses Pengubahan
					$s = $this->user_model->update_user($id);
					if($s){
						$this->template_view->text_view('Sukses mengubah user!', 'Sukses');
					}else{
						$this->template_view->text_view('Gagal mengubah user', 'Gagal');
					}
				}else{
					redirect(base_url('ubah_data/admin/show'));
				}
			}else if($method == "add"){
				if(count($_POST) == 0){
					//Form Pengubahan
					$this->template_view->view('page/tambah_user', NULL, "Tambah Admin", 1);
				}else{
					$r = $this->user_model->add_user(2);
					if($r){
						redirect(base_url('ubah_data/admin'));
					}else{
						$this->template_view->text_view('Gagal menambah user', 'Gagal');
					}
				}
			}else{
				redirect(base_url('ubah_data/admin/show'));
			}
		}

		public function kasir($method = NULL, $id = NULL){
			if($method == "show"){
				$res = $this->user_model->get_user(3);
				$data = array(
					'users' => $res,
					'role' => 'kasir'
				);

				$this->template_view->view('page/data_user', $data, "Data Kasir", 2);
			}else if ($method == "change") {
				if(count($_POST) == 0 && $id != NULL){
					//Form Pengubahan
					$res = $this->user_model->get_specific_user($id);
					$data = array(
						'user' => $res,
						'role' => 'kasir'
					);

					$this->template_view->view('page/ubah_user', $data, "Ubah Data Kasir", 2);
				}else if($id != NULL){
					//Proses Pengubahan
					$s = $this->user_model->update_user($id);
					if($s){
						$this->template_view->text_view('Sukses mengubah user!', 'Sukses');
					}else{
						$this->template_view->text_view('Gagal mengubah user', 'Gagal');
					}
				}else{
					redirect(base_url('ubah_data/kasir/show'));
				}
			}else if($method == "add"){
				if(count($_POST) == 0){
					//Form Pengubahan
					$this->template_view->view('page/tambah_user', NULL, "Tambah Kasir", 2);
				}else{
					$r = $this->user_model->add_user(3);
					if($r){
						redirect(base_url('ubah_data/kasir'));
					}else{
						$this->template_view->text_view('Gagal menambah user', 'Gagal');
					}
				}
			}else{
				redirect(base_url('ubah_data/kasir/show'));
			}
		}
	
	}
	
	/* End of file Ubah_data.php */
	/* Location: ./application/controllers/Ubah_data.php */