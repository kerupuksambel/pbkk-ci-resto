<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_view{
	protected $ci;


	public function __construct(){
        $this->ci =& get_instance();
	}

	public function view($page, $data, $title = NULL, $permission = 3){
		$role = $this->ci->session->userdata('role');

		$template_path = 'template/dashboard';
		$data['page'] = $page;
		$data['title'] = $title;
		if($role <= $permission && $role != 0){
			$this->ci->load->view($template_path, $data);
		}else{
			$data['page'] = 'page/error';
			$data['title'] = 'Error!';
			$this->ci->load->view($template_path, $data);
		}
	}

	public function text_view($text, $title){
		$template_path = 'template/dashboard';
		$data['page'] = $text;
		$data['is_text'] = TRUE;
		$data['title'] = $title;
		$this->ci->load->view($template_path, $data);
	}

	public function public_view($page, $data, $title = NULL){
		$template_path = 'template/dashboard';
		$data['page'] = $page;
		$data['title'] = $title;
		$this->ci->load->view($template_path, $data);
	}

}

/* End of file libraryName.php */
/* Location: ./application/libraries/libraryName.php */
