<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Barang_model extends CI_Model {		
		public function __construct(){
			parent::__construct();
			$config['upload_path'] = './assets/img/item';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 5120;
            $this->load->library('upload', $config);
		}

		public function insert_item(){


			$nama = $this->security->xss_clean($this->input->post('nama'));
			$harga = $this->security->xss_clean($this->input->post('harga'));
			//With images
			// die(var_dump($_FILES['gambar']));
			if($_FILES['gambar'] != NULL){
				if(!$this->upload->do_upload('gambar')){
					$error = $this->upload->display_errors();
					die($error);
				}else{
					$gambar = $this->upload->data()['file_name'];
				}
			}else{
				$gambar = '';
			}

			$data = array(
				'nama_barang' => $nama,
				'harga_barang' => $harga,
				'gambar' => $gambar
			);

			$this->db->insert('barang', $data);

			if($this->db->affected_rows() == 1){
				$ret = array(
					'stat' => TRUE,
					'nama' => $nama,
					'harga' => $harga,
				);
			}else{
				$ret = array(
					'stat' => FALSE ,
					'error' => $error
				);
			}

			return $ret;
		}

		public function get_item(){
			$query = $this->db->get('barang');
			$result = $query->result_array();
			return $result;
		}

		public function get_item_by_id($id = NULL){
			if($id !== NULL){
				$query = $this->db->get_where('barang', array(
					"id" => $id
				));
				$result = $query->result_array();
			}else{
				$result = NULL;
			}

			return $result;
		}

		public function change_item($id = NULL){
			$nama = $this->security->xss_clean($this->input->post('nama'));
			$harga = $this->security->xss_clean($this->input->post('harga'));

            if($_FILES['gambar'] != NULL){
            	$r = $this->upload->do_upload('gambar');
            	if(!$r){
            		return false;
            	}else{
		            $data = array(
		            	'nama_barang' => $nama, 
		            	'harga_barang' => $harga, 
		            	'gambar' => $this->upload->data()['file_name']
		            );
            	}
            }else{
            	$data = array('nama_barang' => $nama, 'harga_barang' => $harga);
            }

            $this->db->where('id', $id);
            $this->db->update('barang', $data);

            if($this->db->affected_rows() > 0){
    			return true;
            }else{
            	return false;
            }
		}

		public function delete_item($id){
			$this->db->where('id', $id);
			$this->db->delete('barang');
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
	
	/* End of file Tambah_model.php */
	/* Location: ./application/models/Tambah_model.php */
	}
?>