<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Kas_model extends CI_Model {
		public function __construct(){
			parent::__construct();
		}
			
		public function insert_trx($nominal){
			date_default_timezone_set("Asia/Jakarta");

			$date = getdate()[0];

			$formatted_date = date("d M Y", $date);
			$q = $this->db->get_where("kas", array("tanggal" => $formatted_date));

			if($q->num_rows() == 0){
				$query_2 = $this->db->insert('kas', array('tanggal' => $formatted_date, 'kas' => $nominal));
			}else{
				$kas_before = $q->result_array()[0]['kas'];
				$kas_before += $nominal;
				$this->db->where('tanggal', $formatted_date);
				$this->db->update('kas', array('kas' => $kas_before));
			}

			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function get_kas_list(){
			$query = $this->db->get('kas');
			$data = $query->result_array();
			return $data;
		}

		public function reset_kas(){
			$query = $this->db->empty_table('kas');
			if($query){
				return true;
			}else{
				return false;
			}
		}
	}
	
	/* End of file Kas_model.php */
	/* Location: ./application/models/Kas_model.php */