<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Login_model extends CI_Model {	
		public function __construct(){
			parent::__construct();
		}

		public function verify_login(){
			$username = $this->security->xss_clean($this->input->post('username'));
			$password = $this->security->xss_clean($this->input->post('password'));

			$q = $this->db->get_where('user', array(
				'username' => $username, 
				'password' => md5($password),
			));

			if($q->num_rows() == 1){
				$r = $q->row();

				$data = array(
					'verified' => TRUE,
					'user' => $r->username,
					'role' => $r->role
				);

				$this->session->set_userdata($data);
			}else{
				$data = array(
					// 'verified' => FALSE
				);
				
				$this->session->set_userdata($data);
			}
		}
	}
	
	/* End of file Login_model.php */
	/* Location: ./application/models/Login_model.php */
?>