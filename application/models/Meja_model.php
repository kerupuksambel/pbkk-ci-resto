<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Meja_model extends CI_Model {	
		public function __construct(){
			parent::__construct();
		}

		public function get_meja(){
			$query = $this->db->get('meja');
			$res = $query->result_array();
			return $res;
		}

		public function get_meja_kosong(){
			$this->db->where('terisi', 0);
			$query = $this->db->get('meja');
			$res = $query->result_array();
			return $res;
		}

		public function fill_meja($id, $total){
			$this->db->where('id', $id);
			$this->db->update('meja', array("terisi" => 1, "tagihan" => $total));
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
		
		public function empty_meja($id){
			$this->db->where('id', $id);
			$this->db->update('meja', array("terisi" => 0, "tagihan" => 0));
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function is_empty_meja($id){
			$this->db->select('terisi');
			$this->db->where('id', $id);
			$q = $this->db->get('meja');
		}

		public function tambah_meja(){
			$this->db->insert('meja', array('nama_meja' => $this->input->post('nama'), 'terisi' => 0));
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function ubah_meja($id){
			$this->db->where('id', $id);
			$this->db->update('meja', array('nama_meja' => $this->input->post('nama')));
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function hapus_meja($id){
			$this->db->where('id', $id);
			$this->db->delete('meja');
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function info_meja($id){
			// $this->db->select('nama_meja');
			$this->db->where('id', $id);
			$q = $this->db->get('meja');

			return $q->result_array()[0];
		}

		public function nama_meja($id){
			$array = $this->info_meja($id);
			return $array['nama_meja'];
		}

		public function tagihan_meja($id){
			$array = $this->info_meja($id);
			return $array['tagihan'];
		}
	}
	
	/* End of file Pesanan_model.php */
	/* Location: ./application/models/Pesanan_model.php */