<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class User_model extends CI_Model {
	
		public function get_user($role){
			$this->db->where('role', $role);
			$q = $this->db->get('user');

			$res = $q->result_array();
			return $res;
		}

		public function get_specific_user($id){
			$this->db->where('id', $id);
			$q = $this->db->get('user');

			$res = $q->result_array()[0];
			return $res;
		}

		public function update_user($id){
			$this->db->where('id', $id);
			$change = array(
				'username' => $this->security->xss_clean($this->input->post('username')),
				'nama' => $this->security->xss_clean($this->input->post('nama')), 
				'alamat' => $this->security->xss_clean($this->input->post('alamat')),
				'ttl' => $this->security->xss_clean($this->input->post('ttl')),
				'nomor_hp' => $this->security->xss_clean($this->input->post('no_hp')),
				'gaji' => $this->security->xss_clean($this->input->post('gaji')),
			);
			if($this->input->post('password') != ""){
				$change['password'] = md5($this->input->post('password'));
			};

			$this->db->update('user', $change);

			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function add_user($role){
			$change = array(
				'username' => $this->security->xss_clean($this->input->post('username')),
				'nama' => $this->security->xss_clean($this->input->post('nama')), 
				'alamat' => $this->security->xss_clean($this->input->post('alamat')),
				'password' => md5($this->input->post('password')),
				'ttl' => $this->security->xss_clean($this->input->post('ttl')),
				'nomor_hp' => $this->security->xss_clean($this->input->post('no_hp')),
				'gaji' => $this->security->xss_clean($this->input->post('gaji')),
				'role' => $role,
			);

			$this->db->insert('user', $change);

			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
	
	}
	
	/* End of file User_model.php */
	/* Location: ./application/models/User_model.php */