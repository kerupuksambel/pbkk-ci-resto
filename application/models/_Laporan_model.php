<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Laporan_model extends CI_Model {
		public function __construct(){
			parent::__construct();
		}

		public function get_kas_list(){
			$query = $this->db->get('kas');
			$data = $query->result_array();
			return $data;
		}

		public function get_kas_with_limit($limit){
			$query = $this->db->get('kas', $limit);
		}
	
	}
	
	/* End of file Laporan_model.php */
	/* Location: ./application/models/Laporan_model.php */