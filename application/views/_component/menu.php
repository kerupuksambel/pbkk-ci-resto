<?php
	$role = $this->session->userdata('role');
	if($role <= 1){
		?>

		<ul id="menu-ceo">
			<li>
				<a href='<?php echo base_url('ubah-data/admin') ?>'>Ubah Data Admin</a>
			</li>
		</ul>

		<?php
	}

	if($role <= 2){
		?>

		<ul id="menu-admin">
			<li>
				<a href='<?php echo base_url('ubah-data/kasir') ?>'>Ubah Data Kasir</a>
			</li>
		</ul>

		<?php
	}

	//Edit Menu
	if($role <= 2){
		?>

		<ul id="manage-admin">
			<li>
				<a href='<?php echo base_url('tambah_barang') ?>'>Input Barang</a>
			</li>
			<li>
				<a href='<?php echo base_url('laporan') ?>'>Laporan</a>
			</li>
		</ul>

		<?php
	}

	if($role <= 3){
		?>

		<ul id="manage-umum">
			<li>
				<a href='<?php echo base_url('kasir')?>'>Kasir</a>
			</li>
		</ul>

		<?php
	}

	?>

	<ul id="umum">
		<li>
			<a href="<?php echo base_url('logout')?>">Logout</a>
		</li>
	</ul>


