Selamat datang, <b><?php echo $name ?></b> dengan jabatan <b>Administrator</b>. Silakan pilih menu yang Anda inginkan di menu atas.

<div class="col-xl-6" id="chart">
    <h3>Pemasukan</h3>
	<canvas id="lineChart"></canvas>
</div>
<canvas id="lineChart" width="400" height="400"></canvas>
<script>
var ctx = document.getElementById('lineChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [<?php foreach ($records as $record){
        	echo "'".$record['tanggal']."',";
        }?>],
        datasets: [{
            label: 'Kas',
            data: [
                <?php foreach ($records as $record){
        	       echo $record['kas'].",";
                } 
                ?>
            ],
            backgroundColor: 'rgba(255,255,255,0)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 3,
            fill: false
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>

