<?php
	if(isset($msg)){
		echo $msg;
	}
?>
<br/>
<form action='<?php echo base_url('login/verify'); ?>' method="POST" class="row">
	<div class="form-group col-md-6">
		<label for="username">Username</label>
		<input type="text" placeholder="Username" name="username" id="username" class="form-control"/>
	</div>
	<div class="form-group col-md-6">
		<label for="password">Password</label>
		<input type="password" placeholder="Password" name="password" id="password" class="form-control" />
	</div>
	<div class="form-group col-md-6">
		<input type="submit" value="Login" class="btn btn-primary">
	</div>
</form>