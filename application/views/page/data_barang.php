<table class="table">
	<tr>
		<th>Nama Barang</th>
		<th>Harga Barang</th>
		<th>Tindakan</th>
	</tr>
	<?php
		foreach ($items as $item):
	?>
	<tr>
		<td><?php echo $item['nama_barang']; ?></td>
		<td><?php echo $item['harga_barang']; ?></td>
		<td><a href="<?php echo base_url('barang/change/'.$item['id']); ?>">Ubah</a> | <a href="<?php echo base_url('barang/delete/'.$item['id']); ?>">Hapus</a></td>
	</tr>
	<?php
		endforeach;
	?>
</table>
<a href="<?php echo base_url('tambah_barang'); ?>" class="btn btn-primary">Tambah Barang</a>