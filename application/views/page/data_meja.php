<table class="table">
	<tr>
		<th>Nama Meja</th>
		<th>Tindakan</th>
	</tr>
	<?php
		foreach ($users as $user):
	?>
	<tr>
		<td><?php echo $user['nama_meja']; ?></td>
		<td><a href="<?php echo base_url('meja/change/'.$user['id']); ?>">Ubah</a> | <a href="<?php echo base_url('meja/delete/'.$user['id']); ?>">Hapus</a></td>
	</tr>
	<?php
		endforeach;
	?>
</table>
<a href="<?php echo base_url('tambah_barang/meja'); ?>" class="btn btn-primary">Tambah Meja</a>