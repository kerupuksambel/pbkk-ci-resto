<table class="table">
	<thead>
		<tr>
			<th>Meja</th>
			<th>Status</th>
			<th>Tindakan</th>
		</tr>
	</thead>
<?php
	foreach ($tables as $table):
?>
	<tr>
		<td><?php echo $table['nama_meja']; ?></td>
		<td><?php echo (($table['terisi'] == 0) ? "Kosong" : "Penuh"); ?></td>
		<td><?php echo (($table['terisi'] == 1) ? "<a href='".base_url('kasir/selesai/'.$table['id'])."'>Selesaikan</a>" : ''); ?></td>
	</tr>
<?php
	endforeach;
?>
</table>