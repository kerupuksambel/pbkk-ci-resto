<table class="table">
	<tr>
		<th>Tanggal</th>
		<th>Pemasukan</th>
	</tr>
	<?php
		foreach ($list as $report):
	?>
	<tr>
		<td><?php echo $report['tanggal']; ?></td>
		<td><?php echo $report['kas']; ?></td>
	</tr>
	<?php
		endforeach;
	?>
</table>
<a href="<?php echo base_url('laporan/reset'); ?>" class="btn btn-danger">Reset Data</a>
<a href="<?php echo base_url('laporan/print'); ?>" class="btn btn-primary">Cetak Data</a>