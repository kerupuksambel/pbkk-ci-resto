<form method="POST" action='<?php echo base_url("pemesanan/struk");?>'>
	Pesanan untuk <b><?php echo $nama_meja ?></b>
	<input type="hidden" name="meja" value="<?php echo $meja ?>">
	<div class="row">
		<?php
			foreach ($items as $item):
		?>
		<div class="item col-md-3">
			<div class="col-md-12">
				<img src="<?php echo base_url('assets/img/item/'); echo ($item['gambar'] != '' ? $item['gambar'] : '../default.jpg'); ?>" style="width: 100%">
			</div>
			<div class="nama"><?php echo $item['nama_barang']; ?></div>
			<div class="harga"><?php echo $item['harga_barang']; ?></div>
			<div class="jumlah form-group">
				<input type="number" value="<?php echo (isset($item['jumlah']) ? $item['jumlah'] : 0); ?>" name="barang_<?php echo $item['id']?>" class="form-control">
			</div>
		</div>
		<?php endforeach; ?>
	</div>
	<input type="submit" value="Pesan" class="btn btn-primary" />
</form>