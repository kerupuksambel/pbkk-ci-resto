<form action="<?php echo base_url('pemesanan/menu');?>" method="POST">
	<table class="table">
		<thead>
			<tr>
				<th>Nama Barang</th>
				<th>Harga Barang</th>
				<th>Jumlah Barang</th>
				<th>Subtotal</th>
			</tr>
		</thead>
		<?php
			$total = 0;
			foreach ($orders as $order):
				$subtotal = $order['harga_barang'] * $order['jumlah'];

				$total += $subtotal;
		?>
		<tr class="order">
			<td class="nama_order"><?php echo $order['nama_barang'] ?></div>
			<input type="hidden" name="nama_<?php echo $order['id']; ?>" value="<?php echo $order['nama_barang']; ?>">
			<td class="harga_order"><?php echo $order['harga_barang'] ?></div>
			<td class="jumlah_order"><?php echo $order['jumlah'] ?></div>
			<input type="hidden" name="barang_<?php echo $order['id']; ?>" value="<?php echo $order['jumlah']; ?>">
			<td class="subtotal"><?php echo $subtotal ?></div>
		</div>

		<?php
			endforeach;
		?>
	</table>

<h5 id="total">Total : <b><?php echo $total; ?></b></h5>
<input type="hidden" name="meja" value="<?php echo $meja ?>">
<div class="form-group">
	<input type="submit" value="Kembali" class="btn btn-danger"/>
	<input type="submit" name="print" value="Print" class="btn btn-primary"/>
</div>
</form>
<form action="<?php echo base_url('pemesanan/konfirmasi'); ?>" method="POST">
	<input type="hidden" name="total" value="<?php echo $total; ?>">
	<input type="hidden" name="meja" value="<?php echo $meja ?>">
	<div class="form-group">
		<input type="submit" value="Selesai" class="btn btn-primary">
	</div>
</form>