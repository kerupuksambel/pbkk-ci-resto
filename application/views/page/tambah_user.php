<form method="POST">
	<div class="form-group">
		<label for="nama">Nama</label>
		<input type="text" name="nama" id="nama" class="form-control" value=""/>
	</div>
	<div class="form-group">
		<label for="alamat">Alamat</label>
		<textarea name="alamat" id="alamat" class="form-control"></textarea>
	</div>
	<div class="form-group">
		<label for="ttl">Tempat dan Tanggal Lahir</label>
		<input type="text" name="ttl" id="ttl" class="form-control" value=""/>
	</div>
	<div class="form-group">
		<label for="hp">Nomor HP</label>
		<input type="text" name="no_hp" id="hp" class="form-control" value=''/>
	</div>
	<div class="form-group">
		<label for="gaji">Gaji</label>
		<input type="text" name="gaji" id="gaji" class="form-control" value=""/>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			<label for="username">Username</label>
			<input type="text" name="username" id="username" class="form-control" value=""/>
		</div>
		<div class="form-group col-md-6">
			<label for="password">Password</label>
			<input type="password" name="password" id="password" class="form-control"/>
			<small class="form-text text-muted">Kosongi bila tidak diganti. Password baru harus berbeda dengan password lama</small>
		</div>
	</div>
	<div class="form-group">
		<input type="submit" value="Submit" class="btn btn-primary"/>
	</div>
</form>