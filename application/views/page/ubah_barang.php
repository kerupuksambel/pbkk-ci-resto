<?php echo form_open_multipart(); ?>
	<div class="form-group">
		<label for="nama_barang">Nama Barang</label>
		<input type="text" class="form-control" id="nama_barang" name="nama" placeholder="Nama Barang" value="<?php echo $data['nama_barang']; ?>" />
	</div>
	<div class="form-group">
		<label for="harga_barang">Harga Barang</label>
		<input type="number" class="form-control" name="harga" id="harga_barang" placeholder="Harga Barang" value="<?php echo $data['harga_barang']; ?>" />
	</div>
	<div class="form-group">
		<label for="gambar">Gambar Barang</label>
		<input type="file" class="form-control" name="gambar" id="gambar" placeholder="Harga Barang"/>
		<small class="text-muted">Kosongkan bila tidak diubah.</small>
	</div>
	<input type="submit" value="Ubah Barang" class="btn btn-primary" />
</form>