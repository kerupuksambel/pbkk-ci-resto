<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Aplikasi Restoran - <?php echo $title ?></title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/chart.js/Chart.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/resto.css'); ?>"/>
<!-- JS Files -->
<script src="<?php echo base_url('assets/vendor/jquery/jquery.slim.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/chart.js/Chart.min.js')?>"></script>
<script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
