<?php
	$role = $this->session->userdata('role');
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
	<div class="container">
		<a class="navbar-brand" href="<?php echo base_url('dashboard') ?>">Aplikasi Restoran</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="nav navbar-nav ml-auto">
				<!-- Pengaturan Akun -->
				<?php if($role <= 2 && $role != 0): ?>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="manage-akun" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Atur Akun
					</a>
					<div class="dropdown-menu" aria-labelledby="manage-akun">
						<?php echo ($role <= 2 && $role != 0 ? '<a class="dropdown-item" href='.base_url('ubah_data/kasir').'>Ubah Data Kasir</a>' : '') ; ?>
						<?php echo ($role <= 1 && $role != 0 ? '<a class="dropdown-item" href='.base_url('ubah_data/admin').'>Ubah Data Admin</a>' : '') ; ?>
					</div>
				</li>
				<?php endif; ?>
				<?php if($role <= 2 && $role != 0): ?>
				<!-- Input Barang -->
				<li class="nav-item">
					<a class="nav-link" href='<?php echo base_url('barang'); ?>'>Atur Barang</a>
				</li>
				<!-- Input Meja -->
				<li class="nav-item">
					<a class="nav-link" href='<?php echo base_url('meja'); ?>'>Atur Meja</a>
				</li>
				<!-- Laporan -->
				<li class="nav-item">
					<a class="nav-link" href='<?php echo base_url('laporan'); ?>'>Laporan</a>
				</li>
				<?php endif; ?>
				<li class="nav-item">
					<a class="nav-link" href='<?php echo base_url('pemesanan'); ?>'>Pemesanan</a>
				</li>
				<?php if($role <= 3 && $role != 0): ?>
				<!-- Kasir -->
				<li class="nav-item">
					<a class="nav-link" href='<?php echo base_url('kasir'); ?>'>Kasir</a>
				</li>
				<!-- Logout -->
				<li class="nav-item">
					<a class="nav-link" href='<?php echo base_url('logout'); ?>'>Logout</a>
				</li>
				<?php endif; ?>
				<?php if($role == 0): ?>
				<!-- Login -->
				<li class="nav-item">
					<a class="nav-link" href='<?php echo base_url('login'); ?>'>Login</a>
				</li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</nav>