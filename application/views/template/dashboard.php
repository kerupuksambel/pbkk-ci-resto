<!DOCTYPE html>
<html lang="en">

<head>
	<?php require "component/include.php"; ?>
</head>

<body>
	<!-- Navigation -->
	<?php require 'component/navbar.php'; ?>

	<!-- Page Content -->
	<div class="container" id="content">
		<h2><?php echo (isset($title) ? $title : ''); ?></h2>
		<?php
			if(isset($is_text)){
				echo $page;
			}else{
				$this->load->view($page);
			}
		?>
	</div> 
</body>

</html>
